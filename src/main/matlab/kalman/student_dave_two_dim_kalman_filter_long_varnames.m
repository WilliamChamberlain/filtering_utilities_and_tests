% 
%Student Dave's tutorial on:  Object tracking in image using 2-D kalman filter
%HEXBUG TRACKING!
%Copyright Student Dave's Tutorials 2012
%if you would like to use this code, please feel free, just remember to
%reference and tell your friends! :)

% William Chamberlain Copyright 2017
% Re-wording of Student Dave's Kalman filter tutorial code 
% from 
%   http://studentdavestutorials.weebly.com/  /   
%   https://www.youtube.com/watch?v=GBYW1j9lC1I  
% with longer variable names and clearer (to me) separation between 
% the prediction-from-estimate step, update-from-measurement step, and 
% the current best estimates.

%here we take the hexbug extracted coordinates and apply the kalman fiter
%to the 2-dimensions of motion  within the image
%to see if it can do better than the tracking alone
%requires matlabs image processing toolbox.

clear all;
close all;
clc;
set(0,'DefaultFigureWindowStyle','docked') %dock the figures..just a personal preference you don't need this.
base_dir = '/mnt/nixbig/ownCloud/project_AA1__1_1/code/practice/hexbug_frames_compressed/';
cd(base_dir);

%% get listing of frames
f_list =  dir('*png');

%% load tracking data
%load('CM_idx_easier.mat') %simple tracking: continuously monitored bug
%load('CM_idx_harder.mat') %hard tracking:  segment of very noisy images and very bad tracking
load('CM_idx_no.mat'); %missing data tracking: segment with no tracking



%% define main variables
S_frame = 200;  %starting frame

dt = 1;         %our sampling rate

u = .005;       % input; acceleration of the robot :  define acceleration magnitude

Q= [CM_idx(S_frame,1); ...
    CM_idx(S_frame,2); ...
                    0; ...
                    0];     % state vector -- four state components: [positionX; positionY; velocityX; velocityY] of the hexbug -- initialised to position at the starting frame ground truth, and zero velocities


tkn_x = 1;                  %measurement noise magnitude in the horizontal direction (x axis).
tkn_y = 1;                  %measurement noise magnitude in the vertical   direction (y axis).
Ez = [  tkn_x       0; ...
        0       tkn_y];    % measurement error/uncertainty covariance

HexAccel_noise_mag = .1;   % for process noise: variation in the input leading to covariance in the state prediction: the variability in how fast the Hexbug is speeding up (stdv of acceleration: meters/sec^2)
Ex = [dt^4/4      0 dt^3/2      0; ...
           0 dt^4/4      0 dt^3/2; ...
      dt^3/2      0   dt^2      0; ...
           0 dt^3/2      0   dt^2].*(HexAccel_noise_mag^2); % Ex convert the process noise (stdv) into covariance matrix  - this is the state error/uncertainty covariance due to process noise

Q_estimate  = Q;  % estimate of initial location estimation of where the hexbug is (what we are updating)
Q_predicted = Q;  % init to the right shape
Q_updated   = Q;  % init to the right shape
P_estimate  = Ex; % estimate of initial Hexbug position variance (covariance matrix)
P_predicted = Ex; % init to the right shape
P_updated   = Ex; % init to the right shape

%% Define update equations in 2-D! (Coefficent matrices): A physics based model for where we expect the HEXBUG to be [state transition (state + velocity)] + [input control (acceleration)]
A = [1  0 dt  0; ...
     0  1  0  dt; ...
     0  0  1  0; ...
     0  0  0  1];       % state transition matrix for the prediction step: model of state response to current state 
 
B = [(dt^2/2); ...
     (dt^2/2); ...
           dt; ...
           dt];         % input response matrix for the prediction step: model of state response to the input commands
       
C = [1 0 0 0; ...
     0 1 0 0];          %this is our measurement function C, that we apply to the state estimate Q to get our expect next/new measurement


%% initize result variables
% Initialize for speed
Q_loc = [];     % ACTUAL hexbug motion path
vel = [];       % ACTUAL hexbug velocity
pos_from_detection_algorithm = []; % the hexbug path extracted by the tracking algorithm

%% initize estimation variables
pos_estimate_hist = []; %  position estimate
vel_estimate_hist = []; % velocity estimate
predicted_state_hist = [];
predicted_covar_hist = [];
r = 5; % r is the radius of the plotting circle
j=0:.01:2*pi; %to make the plotting circle


for t = S_frame:length(f_list)  % running frame-by-frame at a fixed frame rate, so can always do one prediction per frame
    
    % load the image
    img_tmp = double(imread(f_list(t).name));
    img = img_tmp(:,:,1);
    % load the given tracking
    pos_from_detection_algorithm(:,t) = [ CM_idx(t,1); CM_idx(t,2)];
    
    %% do the kalman filter   
    
    % Predict next state estimate - with the last state estimate and predicted motion due to known inputs and response of the Hexbug.
    Q_predicted = A * Q_estimate + B * u;   % a priori state = predicted state = state transition * estimated state  +  input response * input
    Q_estimate  = Q_predicted;              % best estimate is now the a priori
    
    %predict covariance estimate 
    P_predicted = A * P_estimate * A' + Ex;      % a priori covariance = state transition * current state covariance estimate * inv(state transition)  +  covariance on input ('process noise')
    P_estimate  = P_predicted;          % best estimate is now the a priori
        
    % if have a measurement, perform an update step
    if ~isnan(pos_from_detection_algorithm(:,t))   
    
        % updated measurement covariance Kalman Gain
        K = P_estimate*C'*inv(C*P_estimate*C'+Ez);            % a priori Kalman gain = state covariance * trans(measurement) * inv(measurement * state covariance * inv(measurement)  +  measurement error)
                                            %  =  ?? how certain the measurement is ??
        
        % Update the state estimate.
        Q_updated  = Q_estimate  +  ( K * (pos_from_detection_algorithm(:,t)  - C * Q_estimate));  % a posteriori = state updated from measurement = state estimate (predicted or updated) + Kalman gain*state measurement - estimated measurement (=estimated state through the sensor/measurement model)
        Q_estimate = Q_updated;         % best estimate is now the a posteriori         
    
        % update covariance estimate
        P_updated  =  (eye(4)-K*C)*P_estimate;   % a posteriori covariance = updated covariance = ( I - Kalman gain (=confidence in measurement)*measurement model ) * covariance estimate 
        P_estimate =  P_updated;        % best covariance estimate is now the a posteriori
    end
    
    
    
    %% Store data
    pos_estimate_hist = [pos_estimate_hist; Q_estimate(1:2)'];
    vel_estimate_hist = [vel_estimate_hist; Q_estimate(3:4)'];
    predicted_state_hist = [predicted_state_hist; Q_estimate(1)];
    predicted_covar_hist = [predicted_covar_hist; P_estimate];  
   
    %% plot the images with the  tracking
    imagesc(img);
    axis off
    colormap(gray);
    hold on;
    plot(r*sin(j)+pos_from_detection_algorithm(2,t),r*cos(j)+pos_from_detection_algorithm(1,t),'.g'); % the actual tracking
    plot(r*sin(j)+Q_estimate(2),r*cos(j)+Q_estimate(1),'.r'); % the kalman filtered tracking
    hold off
    pause(0.1)
end

